import os, sys
import commands
import logging
import time
import json
import datetime
import errno
import glob

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class myRTT():

  def __init__(self, logger=None):
    self.logger = logger or logging.getLogger(__name__)
    # checkxAOD /eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/rel_1/20.7.X.Y-VAL/x86_64-slc6-gcc49-opt/AtlasDerivation/DerivationFrameworkRTT/Data16JETM1/DAOD_JETM1.Data16.pool.root
    self.checkxAODCommand = "checkxAOD"
    self.basePath = "/eos/atlas/atlascerngroupdisk/proj-sit/rtt/prod/rtt/"
    self.nightlyVer = "rel_0"
    self.release = "20.7.X.Y-VAL" 
    self.cmtConfig = "x86_64-slc6-gcc49-opt"
    self.rttName = "AtlasDerivation/DerivationFrameworkRTT"
    self.derivationName = "Data16JETM1"
    self.xAODName = "DAOD_JETM1.Data16.pool.root"

    # curl command for elastic search
    # !!!! USER/PASS should not be stored here !!!!
    self.curlcmd = "curl -XPOST -u<user/pass> <host>:/<index>/<type> -d "

    self.curlcmd1 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPUT 
https://es-atlas.cern.ch:9203/atlas_rtt-%s -d '{
    "mappings": { "report": { "properties": {
                "input": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}},
                "output": {
                    "properties": {
                        "fileName": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "derivation": {
                            "type": "string",
                            "index": "not_analyzed"}}}}}}}'"""

    self.curlcmd2 = """curl --capath /etc/grid-security/certificates -u '%s:%s' -XPOST https://es-atlas.cern.ch:9203/atlas_rtt-%s/report -d@%s"""

    # Input datafiles
    self.inputDataFile = "root://eosatlas//eos/atlas/user/m/mdobre/forRTTdata15/Data16_207.AOD.pool.root"
    self.inputMCFile = "root://eosatlas//eos/atlas/user/m/mdobre/forRTTmc15/MC15_207.AOD.pool.root"

    # Output datafiles
    self.derivationDict = { 
      "Data16JETM1" : "DAOD_JETM1.Data16.pool.root",
      "Data16JETM2" : "DAOD_JETM2.Data16.pool.root",
      "Data16JETM6" : "DAOD_JETM6.Data16.pool.root",
      "Data16JETM8" : "DAOD_JETM8.Data16.pool.root",
      "Data16JETM9" : "DAOD_JETM9.Data16.pool.root",
      "Data16JETM10" : "DAOD_JETM10.Data16.pool.root",
      "Data16JETM11" : "DAOD_JETM11.Data16.pool.root",

      "MC15JETM1"   : "DAOD_JETM1.MC15.pool.root",
      "MC15JETM2"   : "DAOD_JETM2.MC15.pool.root",
      "MC15JETM3"   : "DAOD_JETM3.MC15.pool.root",
      "MC15JETM4"   : "DAOD_JETM4.MC15.pool.root",
      "MC15JETM5"   : "DAOD_JETM5.MC15.pool.root",
      "MC15JETM6"   : "DAOD_JETM6.MC15.pool.root",
      "MC15JETM7"   : "DAOD_JETM7.MC15.pool.root",
      "MC15JETM8"   : "DAOD_JETM8.MC15.pool.root",
      "MC15JETM9"   : "DAOD_JETM9.MC15.pool.root",
      "MC15JETM10"   : "DAOD_JETM10.MC15.pool.root",
      "MC15JETM11"   : "DAOD_JETM11.MC15.pool.root",

      "Data16SUSY1" : "DAOD_SUSY1.Data16.pool.root", 
      "MC15SUSY1"   : "DAOD_SUSY1.MC15.pool.root", 
      }

    return

  def parseInfo(self, info):
    events = 0
    lines = info.split("\n")
    for count, line in enumerate(lines):
      if "CSV" in line:
        categories = lines[count+1:count+3]
      if (events == 0) and ("Total" in line):
        try:
          events = line.split()[7]
        except:
          events = 0

    categoryDict = {}
    keys = categories[0].split(",")
    values = categories[1].split(",")
    for key, value in zip(keys, values):
      if key == "*Unknown*":
        key = key.replace("*Unknown*", "Unknown")

      categoryDict[key + "_bytes"] = int(round(float(value)*1024))
    
    categoryDict["events"] = int(events)

    return categoryDict

  def getFileSize(self, fileName):
    fileSize = 0
    turlStart = "root://eosatlas/"
    if fileName.startswith(turlStart):
      fileName = fileName.replace(turlStart, "")
    cmd = "eos ls -al " + fileName
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      logger.warning("%s, %s" %(rc, out))

    try:
      fileSize = out.split()[4]
    except:
      fileSize = 0
    
    return fileSize

  def getFileInfo(self, fileName):
    cmd = self.checkxAODCommand + " " + fileName
    self.logger.debug("Command to execute:  %s" % cmd)
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      logger.warning("%s, %s" %(rc, out))

    return rc, out

  def combineInfo(self, fileName):
    contentInfo = {}
    path, fileShort = os.path.split(fileName)
    
    contentInfo["fileName"] = fileShort
    contentInfo["timeStamp"] = datetime.datetime.utcnow().isoformat()
    contentInfo["bytes"] = 0

    # get info from checkxAOD 
    rc, checkxAODInfo = self.getFileInfo(fileName)
    if rc:
      # something went wrong
      return contentInfo
  
    # parse text from checkxAOD - returns a dict
    contentInfo = self.parseInfo(checkxAODInfo)
    # get file size from eos ls
    fileSize = self.getFileSize(fileName)
    contentInfo["bytes"] = int(fileSize)

    return contentInfo

  def getInputFileInfo(self):

    info = {}
    # Data16
    fileName = self.inputDataFile
    contentInfo = self.combineInfo(fileName)
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputData16' 
    info[fileShort] = contentInfo

    # MC15
    fileName = self.inputMCFile
    contentInfo = self.combineInfo(fileName)
    path, fileShort = os.path.split(fileName)
    contentInfo['derivation'] = 'InputMC15' 
    info[fileShort] = contentInfo

    return info

  def getOutputFileInfo(self):

    derivationName = self.derivatioName
    xAODName = self.xAODName

    filePathName = os.path.join(self.basePath, self.nightlyVer, self.release, self.cmtConfig, self.rttName, derivationName, xAODName)
    contentInfo = self.combineInfo(filePathName)
    contentInfo['derivation'] = derivationName

    path, filename = os.path.split(filePathName)
 
    return {filename : contentInfo }

  def getOutputFileInfoAll(self):

    allInfo = {}
    # determine rel_X
    rel = datetime.datetime.today().weekday() + 1
    if rel == 7:
      rel = 0
    relString = "rel_" + str(rel)
    self.logger.debug("Found %s" %relString )

    self.logger.debug("Setting %s" %relString )
    self.nightlyVer = relString

    # Process all data
    for deriv, name in self.derivationDict.iteritems():
      filePathName = os.path.join(self.basePath, relString, self.release, self.cmtConfig, self.rttName, deriv, name)
      contentInfo = self.combineInfo(filePathName)
      contentInfo['derivation'] = deriv
      path, filename = os.path.split(filePathName)
      allInfo[filename] = contentInfo

    return allInfo

  def postToElastic(self, info):
    self.logger.debug("Prepare for elasticsearch")
    #self.logger.debug(info)

    # Create output dir to store json files
    self.logger.debug("If not does not exist, creating output directory %s" %self.nightlyVer)
    path = self.nightlyVer
    try:
      os.makedirs(path)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise
    # Remove old json files
    oldFiles = glob.glob(os.path.join(path,'*.json'))
    self.logger.debug("Removing old files in %s" % oldFiles)
    for f in oldFiles:
      os.remove(f)

    # First find inputData and inputMC
    inputData = {}
    inputMC = {}
    for fileName, content in info.iteritems():
      if content['derivation'] == 'InputData16':
        inputData = content
      if content['derivation'] == 'InputMC15':
        inputMC = content

    # Get credentials
    try:
      from secure import user
      from secure import passwd
    except:
      user = "abc"
      passwd = "abc"
      self.logger.error("User name and password for elasticsearch are missing")

    # Create index mapping for the new day
    todayDate = datetime.datetime.utcnow().date().isoformat()
    cmd = self.curlcmd1 % (user, passwd, todayDate)
    #print cmd
    rc, out = commands.getstatusoutput(cmd)
    if rc:
      logger.warning("New elastic search index mapping command failed with exit code: %s" %(rc))
    
    # Now create input/output json files
    # Start with _1 as suffix of file name 
    num = 1
    for fileName, content in info.iteritems():
      out = {}
      if content['derivation'].startswith("Data16"):
        out['output'] = content
        out['input'] = inputData
      elif content['derivation'].startswith("MC15"):
        out['input'] = inputMC
        out['output'] = content
      
      if out:
        self.logger.debug(out)
        filePathName = os.path.join(path, 'myrtt_%s.json' %num)
        # write out file: 'w' write, 'a' append
        with open(filePathName , 'w') as outfile:
          json.dump(out, outfile)

        # Now post to elasticseach
        cmd = self.curlcmd2 %(user, passwd, todayDate, filePathName)
        #print cmd

        rc, out = commands.getstatusoutput(cmd)
        if rc:
          logger.warning("Post of %s to elasticsearch failed with exit code: %s" %(filePathName, rc))

        num=num+1

    return

  def run(self, argv):

    # Get fileinfo for a single static file
    inputInfo = self.getInputFileInfo()
    #self.logger.debug(inputInfo)

    # Get outfile info for single file
    #outputInfo = self.getOutputFileInfo()
    
    # Get outfile info for a dictionary of files
    outputInfo = self.getOutputFileInfoAll()
    #self.logger.debug(outputInfo)

    # Join input and output file dictionaries
    allInfo = dict(inputInfo.items() + outputInfo.items())
    ##self.logger.debug(allInfo)

    # Upload to elastic search
    self.postToElastic(allInfo)

    return


if __name__ == "__main__":
  app = myRTT()
  app.run(sys.argv)
